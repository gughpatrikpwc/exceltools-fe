import { createStore } from 'vuex'
import acceptedDateFormats from "../AcceptedDateFormats";

export default createStore({
  state() {
    return {
      files: [],
      editedFile: null,
      selectedTableName: '',
      acceptedDateFormats: acceptedDateFormats
    };
  },
  getters: {
    files(state) {
      return state.files;
    },
    editedFile(state) {
      return state.editedFile;
    },
    selectedTableName(state) {
      return state.selectedTableName;
    },
    tableNames(state) {
      return state.editedFile.tables.map(table => table.name);
    },
    selectedTable(state) {
      const table = state.editedFile.tables.find(table => table.name === state.selectedTableName);
      if (table) return table;
    },
    selectedTableHeader(state, getters) {
      return getters.selectedTable.header;
    },
    selectedTableData(state, getters) {
      return getters.selectedTable.data;
    },
    acceptedDateFormats(state) {
      return state.acceptedDateFormats;
    }
  },
  mutations: {
    setFiles(state, payload) {
      state.files = payload;
    },
    setEditedFile(state, payload) {
      state.editedFile = payload;
    },
    setSelectedTableName(state, payload) {
      state.selectedTableName = payload;
    },
    setTableDataCellValue(state, payload) {
      const table = state.editedFile.tables.find(table => table.name === state.selectedTableName);
      if (table)
        table.data[payload.rowIdx][payload.colIdx] = payload.value;
    }
  },
  actions: {
  },
  modules: {
  }
})
