import { createApp } from 'vue';
import App from './App.vue';
import store from './store';
import Toast from "vue-toastification";

import "vue-toastification/dist/index.css";
import "bootstrap-icons/font/bootstrap-icons.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";

const app = createApp(App);

app.use(store);
app.use(Toast, {
    transition: "Vue-Toastification__fade"
});

app.mount('#app');
